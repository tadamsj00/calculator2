package com.tom.calculator2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
public class Calculator2Application {

	public static void main(String[] args) {
		SpringApplication.run(Calculator2Application.class, args);
	}
	@RequestMapping(value="/add", method = RequestMethod.GET)
	@ResponseBody
	public static int addNumbers(@RequestParam("inputNum1") int inputNum1, @RequestParam("inputNum2") int inputNum2)
		{
			return inputNum1+inputNum2;
		}
	
	@RequestMapping(value="/subtract", method = RequestMethod.GET)
	@ResponseBody
	public static int subtractNumbers(@RequestParam("inputNum1") int inputNum1, @RequestParam("inputNum2") int inputNum2)
	{
		return inputNum1-inputNum2;
	}
	
	@RequestMapping(value="/multi", method = RequestMethod.GET)
	@ResponseBody
	public static int multiNumbers(@RequestParam("inputNum1") int inputNum1, @RequestParam("inputNum2") int inputNum2)
	{
		return inputNum1*inputNum2;
	}
	
	/*@RequestMapping(value="/divide", method = RequestMethod.GET)
	@ResponseBody
	public static int divideNumbers(@RequestParam("inputNum1") int inputNum1, @RequestParam("inputNum2") int inputNum2)
	{
		if(inputNum2==0)
		{
			//System.out.println("Division by 0 is not allowed!");
			return 666;
		}
		else
		return inputNum1/inputNum2;
	}*/
}

